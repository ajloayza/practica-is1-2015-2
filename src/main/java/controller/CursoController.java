package controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import domain.Curso;
import repository.jpa.JpaCursoRepository;
import service.CursoService;

@Controller
public class CursoController {
	@Autowired
	CursoService cursoService;

	@RequestMapping(value = "/curso", method = RequestMethod.POST)
	String saveAlumno(@ModelAttribute Curso curso, ModelMap model) {
		System.out.println("savving: " + curso.getId());
		cursoService.save(curso);
		return showCurso(curso.getId(), model);
	}
	@RequestMapping(value = "/add-curso", method = RequestMethod.GET)
	String addNewCurso(@RequestParam(required = false) Long id, ModelMap model) {
		Curso curso = id == null ? new Curso() : cursoService.get(id);
		model.addAttribute("curso", curso);
		return "add-curso";
	}

	@RequestMapping(value = "/mostrar-cursos", method = RequestMethod.GET)
	String showCurso(@RequestParam(required = false) Long id, ModelMap model) {
		if (id != null) {
			Curso curso = cursoService.get(id);
			model.addAttribute("curso", curso);
			return "curso";
		} else {
			Collection<Curso> showCurso = cursoService.getAll();
			model.addAttribute("showCurso", showCurso);
			return "showCurso";
		}
	}
	
	@RequestMapping(value = "/buscarCursoCodigo", method = RequestMethod.GET)
	String buscarCursoCodigo(ModelMap model) {
		return "cursoCodigo";
	}

	
}

