package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import domain.Curso;
import repository.CursoRepository;

@Repository
public class JpaCursoRepository extends JpaBaseRepository<Curso, Long> implements
	CursoRepository{

	@Override
	public Curso buscarPorCodigo(String codigo) {
		String jpaQuery = "SELECT a FROM curso a WHERE a.codigo = :codigo";
		TypedQuery<Curso> query = entityManager.createQuery(jpaQuery, Curso.class);
		query.setParameter("codigo", codigo);
		return getFirstResult(query);
	}

	@Override
	public Collection<Curso> buscarPorNombre(String nombre) {
		String jpaQuery = "SELECT a FROM curso a WHERE a.nombre LIKE ‘%:nombre%’";
		TypedQuery<Curso> query = entityManager.createQuery(jpaQuery, Curso.class);
		query.setParameter("nombre", nombre);
		return query.getResultList();
	}
	
}
