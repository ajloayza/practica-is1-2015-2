package repository;

import java.util.Collection;

import domain.Curso;

public interface CursoRepository extends BaseRepository<Curso, Long> {
	
	Curso buscarPorCodigo(String codigo);

	Collection<Curso> buscarPorNombre(String nombre);
}

