package service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.AlumnoRepository;
import domain.Alumno;

@Service
public class AlumnoService {
	
	@Autowired 
	AlumnoRepository alumnoRepository;
	
	@Transactional
	public void save(Alumno alumno) {
		if (alumno.getId() == null) {
			alumnoRepository.persist(alumno);
		} else {
			alumnoRepository.merge(alumno);
		}
	}

	public Alumno get(Long id) {
		return alumnoRepository.find(id);
	}

	public Collection<Alumno> getAll() {
		return alumnoRepository.findAll();
	}
	
}
